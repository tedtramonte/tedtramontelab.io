import Isotope from 'isotope-layout';
import imagesLoaded from 'imagesloaded';
import PhotoSwipeLightbox from 'photoswipe/lightbox';
import PhotoSwipe from 'photoswipe';

let grid = new Isotope('.photo-grid', {
    itemSelector: '.photo-grid-item',
    layoutMode: 'masonry',
    percentPosition: true,
    masonry: {
        horizontalOrder: true,
    }
});

imagesLoaded(document.querySelector('.photo-grid'), function () {
    grid.arrange();
});

const lightbox = new PhotoSwipeLightbox({
    gallery: '#photogallery',
    children: 'a',
    pswpModule: PhotoSwipe
});
lightbox.init();
