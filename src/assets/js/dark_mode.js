let darkModeToggle = document.getElementById('darkModeToggle');
let pefersColorSchemeDarkQuery = window.matchMedia ? window.matchMedia('(prefers-color-scheme: dark)') : null;
let pefersColorSchemeLightQuery = window.matchMedia ? window.matchMedia('(prefers-color-scheme: light)') : null;


function getUserSelectedTheme() {
    return localStorage.getItem('theme');
}


function getSystemTheme() {
    if (window.matchMedia) {
        if (pefersColorSchemeDarkQuery.matches) {
            return 'dark';
        }
        else if (pefersColorSchemeLightQuery.matches) {
            return 'light';
        }
        else {
            return null;
        }
    }
    return null;
}


function setTheme(theme = null) {
    if (theme !== null) {
        localStorage.setItem('theme', theme);
    }
    else {
        // Probably don't want to remove the local storage preference once it's set
        // the user toggled it off on purpose
        localStorage.removeItem('theme');
    }
}


function applyTheme() {
    _userSelectedTheme = getUserSelectedTheme();
    if (_userSelectedTheme == null) {
        _systemTheme = getSystemTheme();
    }

    // Check if user has a preference saved already
    if (_userSelectedTheme !== null) {
        setTheme(_userSelectedTheme);
        _userSelectedTheme == 'dark' ? darkModeToggle.checked = true : darkModeToggle.checked = false;
        document.body.setAttribute('data-theme', _userSelectedTheme);
    }
    // Check if System has a preference
    else if (_systemTheme !== null) {
        _systemTheme == 'dark' ? darkModeToggle.checked = true : darkModeToggle.checked = false;
        document.body.setAttribute('data-theme', _systemTheme);
    }
    // User has no preference and Media-Queries are not supported
    else {
        // Default
    }
}


// Listen for changes to System preference
if (window.matchMedia) {
    pefersColorSchemeDarkQuery.addEventListener('change', function () {
        applyTheme();
    });
}


window.addEventListener('load', function () {
    if (darkModeToggle) {
        applyTheme();
        darkModeToggle.addEventListener('change', function () {
            darkModeToggle.checked ? setTheme('dark') : setTheme('light');
            applyTheme();
        });
    }
});
