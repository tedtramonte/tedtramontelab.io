import 'bootstrap';
import hljs from 'highlight.js/lib/common';
import django from 'highlight.js/lib/languages/django';
import imagesLoaded from 'imagesloaded';

hljs.registerLanguage('django', django);
hljs.highlightAll();

imagesLoaded(document.getElementsByClassName("dyn-img-link"), function () {
  let img_links = document.getElementsByClassName("dyn-img-link");
  for (let i = 0; i < img_links.length; i++) {
    let img = img_links[i].firstElementChild.lastElementChild;
    img_links[i].href = img.currentSrc || img.dataset.src;
  }
});

let snoop_message = `
         ###################
         *7****7####M*****7*
               |####M
  |###         |####M
  |###         |####M
  ]###.        |####M                               Ted Tramonte, LLC
#########M     |####M
**%###C**"     |####M                                  Hey, there!
  |###         |####M             If you're the type to pop open the hood of my website,
  |###         |####M              there's a very good chance I'd like to talk to you,
  |###         |####M                              work related or not.
  |###         {####M
  |###         #####M                      https://www.tedtramonte.com/contact
  |###b       [####b
    ###M....,,####C
     |####m#####M
`;

window.addEventListener('load', function () {
  console.log(snoop_message);
});
