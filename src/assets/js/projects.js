import Isotope from 'isotope-layout';
import imagesLoaded from 'imagesloaded';


let grid = new Isotope('.project-grid', {
    itemSelector: '.project-grid-item',
    layoutMode: 'masonry',
    percentPosition: true,
    masonry: {
        horizontalOrder: true,
    }
});

imagesLoaded(document.querySelector('.project-grid'), function () {
    grid.arrange();
});

let filtersButtonGroup = document.querySelector('.filters-button-group');
let filters = filtersButtonGroup.getElementsByTagName('input');
for (let i = 0; i < filters.length; i++) {
    filters[i].addEventListener('click', function (event) {
        document.querySelector('#noResultsMsg').classList.add('d-none');

        let _filterValue = '';
        for (let i = 0; i < filters.length; i++) {
            if (filters[i].checked) {
                _filterValue += filters[i].getAttribute('data-iso-filter');
            }
        }

        grid.arrange({ filter: _filterValue });
    });
}

grid.on('layoutComplete', function (filteredItems) {
    if (!filteredItems.length) {
        document.querySelector('#noResultsMsg').classList.remove('d-none');
    }
});
