function makeImageObject(path, position, alt_text) {

    return {
        path: path,
        position: position, // top, bottom, float
        alt_text: alt_text,
    }
}

// Project structure
// TODO: There's probably a better way to do this
// {
//     title: '',
//     date: '',
//     description: [
//         '',
//     ],
//     technologies: [
//         '',
//     ],
//     link: {
//         href: '',
//         text: ''
//     },
//     tags: [
//         '',
//     ],
//     image: makeImageObject('', '', ''),
//     featured: false,
// },

module.exports = {
    projects: [
        {
            title: 'FSW Infrastructure Overhaul',
            date: 'December 2021',
            description: [
                'Between 2018 and 2020, Docker gained traction among my coworkers and in early 2021 I lead a pseudo-committee meeting weekly to build out an entire replacement infrastructure for the relatively archaic application infrastructure at the college.',
                'Ansible formed the foundation of this overhaul; all provisioning of physical and virtual machines had to be done by Ansible playbooks stored in Git. These playbooks would create a k3s flavored Kubernetes cluster across a mix of physical servers and virtual machines spread through different datacenters to ensure the highest degree of fault tolerance. Besides services critical to bootstrapping the cluster, all workloads would be deployed and continuously updated by ArgoCD. We chose Rook Ceph to provide persistent block and S3 storage to workloads in the cluster.',
                'Finally, in December 2021, the proverbial switches were flipped, and all FSW website traffic was pointed to the new infrastructure. To our delight, the application responded to this massive load by gracefully scaling up to meet the demand exactly as planned.',
            ],
            technologies: [
                'Ansible',
                'GitLab CI/CD',
                'Kubernetes - k3s, ArgoCD, Rook Ceph, and much more',
            ],
            tags: [
                'featured',
                'kubernetes',
            ],
            featured: true,
        },
        {
            title: 'TabooBot',
            date: 'November 2021',
            description: [
                'A Discord bot to help your friends stop using certain words or phrases in conversation. Born out of the need to censure the neverending cryptocurrency discussion in our social Discord server, I chose to write this bot in Rust rather than Python for several reasons:',
                '<ul><li>discord.py\'s maintainer announced they were leaving</li><li>Serenity had support for cutting edge "slash commands"</li><li>Rust evangelists won</li></ul>',
                'The development experience was an absolute joy and I now consider myself one of the aforementioned Rust evangelists.',
            ],
            technologies: [
                'Rust - Serenity',
                'Redis',
                'Kubernetes'
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/taboobot',
                text: 'View on GitLab'
            },
            tags: [
                'featured',
                'rust',
                'redis',
                'kubernetes',
            ],
            image: makeImageObject('img/projects/logo-taboobot.png', 'float', 'TabooBot logo'),
            featured: true,
        },
        {
            title: 'State Plates',
            date: 'July 2021',
            description: [
                'A simple web application to track license plates you see on a road trip written using Vue.js.',
                'While on a long summer roadtrip, my wife and I found it hard to keep track of which state license plates we had seen. Could we have kept a note on one of our phones? Sure, but that would be too easy. Weather kept me inside for most of our vacation, so I took the opportunity to put this little application together using Vue, which I had always been curious to explore.',
                'I hated it. I can appreciate that Vue is probably great, but I think I\'m very much wired to think in terms of React and I would need a lot longer than a few days of vacation to fully appreciate the Vue mindset.',
                'You can <a href="https://tedtramonte.gitlab.io/state-plates/" target="_blank" rel="noopener">play State Plates here</a>.'
            ],
            technologies: [
                'JavaScript - Vue.js',
                'GitLab Pages',
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/state-plates',
                text: 'View on GitLab'
            },
            tags: [
                'js',
            ],
        },
        {
            title: 'GoSolo',
            date: 'March 2021',
            description: [
                'GoSolo is safe and simple way to get into a solo public session of Grand Theft Auto Online written in Go. GoSolo simply suspends the GTA5 process for 10 seconds and then resumes it. If run on Windows, GoSolo will attempt to bring the Grand Theft Auto window back into focus as a convenience.',
                'Originally, GoSolo was a small Python script. It worked, but would take two seconds to begin running. I figured a compiled binary would have to begin execution faster than two seconds and so I dove into rewriting the script in Go. The results were inconclusively faster but certainly better. I was able to automatically refocus the game window after executing and distribute GoSolo to my non-developer friends for their own usage without asking them to install Python first.',
                'Along the way, I <a href="https://github.com/goreleaser/goreleaser/pull/2122" target="_blank" rel="noopener">contribtued a rewrite</a> to <a href="https://goreleaser.com/" target="_blank" rel="noopener">GoReleaser</a>\'s <a href="https://goreleaser.com/ci/gitlab/" target="_blank" rel="noopener">GitLab CI documentation</a>.',
            ],
            technologies: [
                'Go',
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/gosolo',
                text: 'View on GitLab'
            },
            tags: [
                'go',
            ],
        },
        {
            title: 'Valheim Server',
            date: 'February 2021',
            description: [
                'Valheim Server is an OCI container image for hosting a dedicated server for the game <a href="https://store.steampowered.com/app/892970/Valheim/" target="_blank" rel="noopener">Valheim</a>. It is automatically rebuilt and published using my project, <a href="https://gitlab.com/tedtramonte/steamtrigger" target="_blank" rel="noopener">Steamtrigger</a>, when the developers release updates to the stable version of the game.',
            ],
            technologies: [
                'Steamtrigger',
                'Docker',
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/valheim-server',
                text: 'View on GitLab'
            },
            tags: [
                'docker',
            ],
        },
        {
            title: 'Steamtrigger',
            date: 'February 2021',
            description: [
                'Steamtrigger is an  OCI container image for triggering a GitLab CI/CD Pipeline based on Steam application activity. The underlying script relies on a Steam changenumber websocket from <a href="https://github.com/xPaw/SteamWebPipes" target="_blank" rel="noopener">xPaw\'s SteamWebPipes</a> and Steam data lookups fron <a href="https://www.steamcmd.net/" target="_blank" rel="noopener">Jona Koudijs\' steamcmd.net</a>.',
                'When the video game <a href="https://store.steampowered.com/app/892970/Valheim/" target="_blank" rel="noopener">Valheim</a> released in early access, I was one of the first to publish an image of the dedicated multiplayer server to Docker Hub. Games in early access tend to move fast and break things and I was not interested in manually rebuilding the server image every time they updated. After doing some research and concluding that nothing existed currently to make this process easy, I pulled together several services and built Steamtrigger which will monitor Steam for updates to the game and trigger <a href="https://gitlab.com/tedtramonte/valheim-server" target="_blank" rel="noopener">valheim-server</a>\'s CI/CD pipeline to rebuild, tag, and publish new container images.',
            ],
            technologies: [
                'Python',
                'Docker',
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/steamtrigger',
                text: 'View on GitLab'
            },
            tags: [
                'featured',
                'python',
                'docker',
            ],
            featured: true,
        },
        {
            title: 'Lynching in LaBelle',
            date: 'April 2021',
            description: [
                'A digital history project by Dr. Brandon Jett chronicling the murder of a young man in LaBelle, Florida.',
                'Dr. Jett wanted a website that could centralize, organize, and display historical information about the lynching in order to provide easy access to that knowledge for the public. After reaching out to Florida SouthWestern State College for insight into what this project might entail, Dr. Jett asked me to put the website together. Since I imagined the information needing some updating over the years and knowing the professor would be capable of adding content himself, I selected WordPress as the underlying platform for the project.',
            ],
            technologies: [
                'WordPress',
            ],
            link: {
                href: 'https://lynchinginlabelle.com',
                text: 'View website'
            },
            tags: [
                'wordpress',
            ],
            image: makeImageObject('img/projects/screenshot-lynchinginlabelle.png', 'top', 'Lynching in LaBelle screenshot'),
        },
        {
            title: 'Pyomodoro',
            date: 'October 2020',
            description: [
                'An easy to use CLI for the <a href="https://francescocirillo.com/pages/pomodoro-technique" target="_blank" rel="noopener">Pomodoro Technique</a> written in Python.',
                'I wanted to experiment with writing a CLI application and was also curious about the process of publishing a package to the Python Package Index. A pomodoro timer seemed to be the perfect project to explore both of those areas.',
                'I no longer maintain this project.'
            ],
            technologies: [
                'Python - Click, plyer',
                'Docker',
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/pyomodoro',
                text: 'View on GitLab'
            },
            tags: [
                'featured',
                'python',
                'docker',
            ],
            image: makeImageObject('img/projects/logo-pyomodoro.png', 'float', 'Pyomodoro logo'),
            featured: true,
        },
        {
            title: 'Tedflix',
            date: 'August 2020',
            description: [
                'A total video streaming service similar to Netflix for me to watch at home and on the go.',
                'Originally run on an enterprise Dell R710, I have since migrated the platform to more modern desktop hardware while still using a Dell MD1000 to provide 50+TB of storage.',
                'While no individual part is my own work, configuring each microservice to work together forming a complete platform was a true test of my systems integration and troubleshooting skills.',
            ],
            technologies: [
                'Docker-Compose',
                'GitLab CI/CD, GitLab Runner',
            ],
            link: {
                href: 'https://gitlab.com/door-to-darkness/tedflix',
                text: 'View on GitLab'
            },
            tags: [
                'featured',
                'docker',
            ],
            image: makeImageObject('img/projects/logo-tedflix.png', 'float', 'Tedflix logo'),
            featured: true,
        },
        {
            title: 'Bait or Not',
            date: 'June 2020',
            description: [
                'A Twitter bot that judges whether a tweet is "bait" or "not bait."',
                'Mention <a href="https://twitter.com/baitornot" target="_blank" rel="noopener">@Baitornot on Twitter</a>, whether in a top level tweet or in reply to a tweet, and the bot will quote reply with its judgement.',
            ],
            technologies: [
                'Python - Tweepy',
                'Docker, Kubernetes',
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/baitornot',
                text: 'View on GitLab'
            },
            tags: [
                'python',
                'docker',
                'kubernetes',
            ],
            image: makeImageObject('img/projects/screenshot-baitornot.png', 'top', 'Bait or Not screenshot'),
        },
        {
            title: 'Florida SouthWestern State College CARES Act Dashboard',
            date: '2020',
            description: [
                'A dashboard for Florida SouthWestern State College employees to easily disburse CARES Act grant funds to students.',
                'As part of the CARES Act, Florida schools recieved around $4,000,000 to aid students who were financially impacted by the COVID-19 pandemic. The Office of Financial Aid requested IT\'s assistance to get the funds in the hands of students as soon as possible.In roughly a week, we created an automatically populated form for the students, a RESTful API for the form submissions, and a small React dashboard to display that data.On the dashboard, Financial Aid employees can batch process the submitted applications, review the applications that failed some prerequisite conditions, manually adjust an application\'s status or cash award, and forward the approved applications to Accounts Recieveable to disburse the funds.',
            ],
            technologies: [
                'PHP - CodeIgniter',
                'JavaScript - React',
                'MariaDB',
                'Docker',
            ],
            tags: [
                'php',
                'js',
                'sql',
                'docker',
            ],
            image: makeImageObject('img/projects/screenshot-caresact.png', 'top', 'CARES Act Dashboard screenshot'),
        },
        {
            title: 'FSW x Olark Integration',
            date: 'April 2020',
            description: [
                'Olark is live chat software that includes Chatbots and customer data tools.',
                'As COVID-19 pushed Florida SouthWestern State College into a fully virtual experience, we saw an immediate need for more real-time methods of communication besides the phone. After evaluating our options, we chose to integrate Olark, as their API documentation indicated we would be able to hack together a Single Sign-On type of flow where chat users could verify their identity to the chat agent, allowing the agent to discuss sensitive details that would otherwise violate FERPA.',
                'I\'m very proud of the clever code we came up with to make the process seamless for both the users and agents, and Olark found our implementation intriguing enough to interview us and write a <a href="https://web.archive.org/web/20210915162509/https://www.olark.com/case-studies/florida-southwestern-state-college/" target="_blank" rel="noopener">case study, which has much more impressive details</a  target="_blank" rel="noopener">.',
            ],
            technologies: [
                'JavaScript',
            ],
            link: {
                href: 'https://web.archive.org/web/20210915162509/https://www.olark.com/case-studies/florida-southwestern-state-college/',
                text: 'Read Olark\'s Case Study'
            },
            tags: [
                'featured',
                'js',
            ],
            featured: true,
        },
        {
            title: 'FSW Alert',
            date: 'June 2019',
            description: [
                'The FSW Alert emergency notification system is the official source for emergency alerts, warnings, and information from Florida SouthWestern State College. FSW Alert delivers emergency alerts to students, faculty, staff, and campus partners simultaneously through 20 primary and secondary delivery methods in 5 minutes or less.',
                'Florida SouthWestern State College had a solution in place to send emergency notifications by phone and text, but no way to extend those notifications to places where it was "impossible to miss." Beginning in April 2019, work began to expand FSW\'s emergency notifications to include the FSW website, Canvas website, and the FSW App.',
                'The solution involved PHP acting as a proxy API and, for most of the new clients, a vanilla JavaScript script. On the day of our planned initial test, a severe tornado warning was issued near the campus. This necessitated <em>actually</em> using the system, rather than testing it. To our relief, every piece worked without issue. The new FSW Alert system would go on to prove itself invaluable in communicating 2020\'s rapidly changing COVID-19 situtation.'
            ],
            technologies: [
                'PHP - CodeIgniter',
                'JavaScript',
            ],
            tags: [
                'php',
                'js',
            ],
            image: makeImageObject('img/projects/logo-fswalert.png', 'float', 'FSW Alert logo'),
        },
        {
            title: 'Sow Joan',
            date: 'April 2019',
            description: [
                'A Discord bot for Animal Crossing: New Horizons that enables easy coordination of turnip prices.',
                'Users can register the islands that they play on, report their turnip prices for their islands, and pull a timezone aware list of prices from every user in a Discord server to find the best price available to buy or sell turnips at!',
                'I no longer maintain this project.',
            ],
            technologies: [
                'Python - Discord.py, Django, Django Rest Framework, pytz',
                'PostgreSQL',
                'Docker',
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/SowJoan',
                text: 'View on GitLab'
            },
            tags: [
                'python',
                'sql',
                'docker',
            ],
            image: makeImageObject('img/projects/logo-sj.png', 'float', 'Sow Joan logo'),
        },
        {
            title: 'Florida SouthWestern State College CMS',
            date: '2019-2022',
            description: [
                'A bespoke content management system aimed at meeting the unique needs of a university.',
                'Moving the FSW website to a CMS has been a long term goal of mine since I started working there in 2016. After assuming the role of webmaster, our web team trialed several available CMS solutions like ExpressionEngine, Wordpress, Drupal, and others. While they each had their benefits, various team members had reservations about each.',
                'In the end, we disregarded all common advice and decided to forge ahead with our own custom CMS built on Laravel as the backend and React as the frontend.',
            ],
            technologies: [
                'PHP - Laravel',
                'JavaScript - React (and many supporting packages)',
                'MariaDB',
                'Docker',
            ],
            tags: [
                'php',
                'js',
                'sql',
                'docker',
            ],
            image: makeImageObject('img/projects/screenshot-fswcms.png', 'top', 'FSW CMS screenshot'),
        },
        {
            title: 'www.tedtramonte.com',
            date: 'July 2018',
            description: [
                'A static website for <em>Ted Tramonte, LLC</em> doubling as my living resum&eacute;. Eleventy is an awesome static site generator which is perfect for powering a simple marketing site like this. Since I\'m prone to breaking my own infrastrucure and it would be embarrassing for my own website to 404, I host the website using GitLab pages. If it\'s broken, it\'s their fault.',
            ],
            technologies: [
                'JavaScript - Eleventy',
                'GitLab Pages',
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/tedtramonte.gitlab.io',
                text: 'View on GitLab'
            },
            tags: [
                'js',
            ],
            image: makeImageObject('img/logo-black.png', 'float', 'Ted Tramonte, LLC logo'),
        },
        {
            title: 'Talented Discord',
            date: 'April 2018',
            description: [
                'A Discord bot for Heroes of the Storm that provides builds for a hero, timed announcements for match coordination, patchnotes, and more!',
                'This is the project that really started it all. A lot of my beginner\'s impostor syndrome melted away while building Talented Discord. Challenges in development and production lead me to containerization and orchestration and ultimately got me interested in DevOps as a discipline.',
                'I no longer maintain this project.',
            ],
            technologies: [
                'Python - Discord.py, Django, Django Rest Framework, Beautiful Soup 4',
                'JavaScript - React, Axios',
                'PostgreSQL',
                'Docker',
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/TalentedDiscord',
                text: 'View on GitLab'
            },
            tags: [
                'featured',
                'python',
                'js',
                'sql',
                'docker',
            ],
            image: makeImageObject('img/projects/logo-td-light.png', 'float', 'Talented Discord logo'),
            featured: true,
        },
        {
            title: 'Florida SouthWestern State College Redesign',
            date: 'February 2019',
            description: [
                'An accessibility-focused visual and marketing overhaul for the Florida SouthWestern State College (FSW) website.',
                'I started working at FSW in 2016 shortly after the webmaster rolled out a new design using a Wordpress theme converted for use in CodeIgniter. The ensuing three years would see me replacing that webmaster and planning out how to slim down and modernize the gargantuan CSS, countless JS scripts, and 30MB of images on the homepage.',
                'By trimming down the styles and scripts to only Bootstrap, making full use of a CodeIgniter templating system, and optimizing image sizes and resolutions, the redesigned website homepage is roughly 2MB in total and loads twice as fast.',
                '<a href="https://web.archive.org/web/20190204064419/https://www.fsw.edu/" target="_blank" rel="noopener">Check out the website\'s previous design for a comparison</a>.The Wayback Machine may not load every single image that was actually on the page, so you\'ll have to just trust me on that one.',
            ],
            technologies: [
                'PHP - CodeIgniter',
                'Bootstrap 4',
            ],
            tags: [
                'php',
            ],
            image: makeImageObject('img/projects/screenshot-fsw.png', 'bottom', 'FSW Redesign screenshot'),
        },
        {
            title: 'Shake!',
            date: 'May 2017',
            description: [
                'A microgame in the style of Warioware created in Game Maker Studio 2.',
                '<em>Shake!</em> was my entry to <a href="https://forum.yoyogames.com/index.php?threads/the-micro-jam-theme-crazy-party.25381/" targe="_blank" rel="noopener">a community-member ran Game Jam themed around microgames</a> and my first ever released game. <em>Shake!</em> won third place.'
            ],
            link: {
                href: 'https://gitlab.com/tedtramonte/MicroJam',
                text: 'View on GitLab'
            },
            tags: [
                'game',
            ],
            image: makeImageObject('img/projects/screenshot-shake.png', 'top', 'Shake! screenshot'),
        },
        {
            title: 'Dream Catchers Super Store',
            date: '2015-2016',
            description: [
                'A fully featured e-commerce website built from scratch.',
                'For my Florida Gulf Coast University Senior Capstone project, my two-man team was assigned to create an e-commerce website similar to Walmart.com or Amazon.com. We could use any backend languages to get the job done, as long as there were no frameworks involved. After delivering the initial project, our team was matched up with another team and challenged to integrate our two websites. Our final project featured daily sales, product categorizing and sorting, product reviews, a shopping cart, order invoicing, and user accounts.'
            ],
            technologies: [
                'PHP',
                'MySQL',
            ],
            tags: [
                'php',
                'sql',
            ],
            image: makeImageObject('img/projects/screenshot-dreamcatchers.png', 'top', 'Dream Catchers Super Store screenshot'),
        },
    ]
};
