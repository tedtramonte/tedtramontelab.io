---
title: Highly Available MariaDB Galera Cluster in Bare Metal k3s Kubernetes
description: How Florida SouthWestern State College brought MariaDB Galera to its on-campus bare metal k3s kubernetes cluster.
date: 2021-07-01T13:17:57Z
last_updated:
tags:
  - technology
  - mariadb
  - galera
  - metallb
  - k3s
  - traefik
  - kubernetes
---
<p>While architecting our first kubernetes cluster at Florida SouthWestern State College (FSW), we had many decisions to make to ensure we finished with a feature filled and production ready environment. Perhaps the biggest decisions we needed to make was how to handle our existing database. At the time, all of FSW's web applications relied on an aging MariaDB Galera cluster. After giving it some thought, we decided that running MariaDB in a container was fundamentally no different from running directly on a host since our backup processes could still reach the data. So, we made the choice to bring up MariaDB Galera in our cluster to provide database services to our applications.</p>
<p><mark>Our goal was to have access to a highly available and replicated MariaDB Galera cluster from inside and outside of our bare metal MetalLB load balanced k3s kubernetes cluster.</mark> The installation was simple enough using <a href="https://github.com/bitnami/charts/tree/master/bitnami/mariadb-galera" target="_blank" rel="noopener">Bitnami's MariaDB Galera Helm chart</a>. The only thing keeping us from finishing the deployment was exposing the application to our network, which was a problem. FSW's network network is highly utilized and IP reservations are at a premium. Our MetalLB installation was configured to hand out precisely one IP address we were cleared to use. k3s' installation of Traefik had already claimed this IP for its <code>LoadBalancer</code>. Securing another was potentially impossible. How could we get MariaDB Galera deployed in this uniquely limited scenario?</p>

<details class="alert alert-info">
  <summary>TL;DR Solution Spoiler</summary>
  We settled on <a href="#solution">MetalLB IP address sharing</a>.
</details>

<h2>Attempt #1: Traefik</h2>
<p>Since Traefik was already in the cluster, that was our first stop.</p>

<figure class="text-center">
  <blockquote class="blockquote">
    <p>Traefik ... is a modern HTTP reverse proxy and load balancer that makes deploying microservices easy. Traefik integrates with your existing infrastructure components ... and configures itself automatically and dynamically. <mark>Pointing Traefik at your orchestrator should be the only configuration step you need.</mark></p>
  </blockquote>
  <figcaption class="blockquote-footer">
    <cite title="Traefik's GitHub README.md"><a href="https://github.com/traefik/traefik" target="_blank" rel="noopener">Traefik's GitHub README.md</a></cite>
  </figcaption>
</figure>

<p>I like Traefik a lot, until I have to turn to their documentation. Since Traefik can talk to so many different types of infrastructure, there are pages upon pages of documentation on how to achieve a particular goal. This is aggravated by the transition from Traefik 1.x to Traefik 2.x leaving conflicting statements and Google results laying around. Overall, things are not great for the user who just needs to get things done.</p>
<p>My first instinct was to simply (naively) create an <code>IngressRoute</code> in the cluster pointing to the <code>ClusterIP</code> service deployed by the MariaDB Galera Helm chart. That will do the job in almost every case. However, the more networking savvy members of the team pointed out that MariaDB doesn't speak HTTP, only TCP.</p>
<p>Fortunately, Traefik can speak TCP directly with an <code>IngressRouteTCP</code>. After realizing that Traefik's TCP Routers only have one possible routing rule (<code>HostSNI</code>) which can only be read from TLS requests, which MariaDB also doesn't speak, but that <code>HostSNI(`*`)</code> would allow non-TLS rquests to use that router(<a href="https://doc.traefik.io/traefik/routing/routers/#rule_1" target="_blank" rel="noopener">seriously, look at this madness</a>), traffic was <em>still</em> unable to reach the database from outside of the cluster.</p>
<p>In a moment of clarity, we realized that k3s' Traefik installation ships with two external entrypoints for ports 80 and 443, but none for MariaDB's port 3306. After playing around with different ways of configuring a new entrypoint on k3s' installation of Traefik, we still saw HTTP 404 responses rather than TCP traffic, meaning we were still talking to Traefik rather than MariaDB.</p>
<p>We decided to start investigating other solutions.</p>

<h2>Attempt #2: NodePort</h2>
<p><code>NodePort</code> services are the easiest way to expose an application outside of your cluster. The port you specify is opened on every node in the cluster and mapped to the application's port. Trying this was as easy as modifying our MariaDB Galera values to use a <code>NodePort</code> rather than <code>ClusterIP</code>.</p>
<p>While this worked, there was no load balancing. Clients had to connect with a specific <code>NodeIP:port</code> combination and would always connect to the pod on that node. To reach a different pod, clients had to manually switch IPs, and that's not highly available.</p>

<span id="solution" class="anchor"></span>
<h2>Solution: MetalLB IP Address Sharing</h2>
<p>Finally, we turned our attention back to MetalLB. Since our IP address pool is limited, we needed to find a way to reuse the IP that Traefik uses.</p>

<figure class="text-center">
  <blockquote class="blockquote">
    <p>By default, <code>Services</code> do not share IP addresses. <mark>If you have a need to colocate services on a single IP, you can enable selective IP sharing by adding the <code>metallb.universe.tf/allow-shared-ip</code> annotation to services.</mark></p>
  </blockquote>
  <figcaption class="blockquote-footer">
    <cite title="MetalLB - Usage"><a href="https://metallb.universe.tf/usage/#ip-address-sharing" target="_blank" rel="noopener">MetalLB - Usage</a></cite>
  </figcaption>
</figure>

<p>According to <a href="https://metallb.universe.tf/usage/#ip-address-sharing" target="_blank" rel="noopener">MetalLB's documentation</a>, it is possible to <i>colocate</i> services on the same IP in situations where certain conditions are met. After reviewing our needs, we decided we met these conditions. In order to start sharing our single IP address between Traefik and MariaDB Galera, we had to set <strong>annotations with the same value</strong> on their respective services.</p>

<h3>Configuring k3s Deployed Traefik for MetalLB IP Sharing</h3>
<p>Our first task was to modify k3s' installation of Traefik to have a specific annotation on its <code>LoadBalancer</code> service. While attempting to configure an <code>IngressRouteTCP</code> above, I learned that <a href="https://rancher.com/docs/k3s/latest/en/helm/#customizing-packaged-components-with-helmchartconfig" target="_blank" rel="noopener">Rancher has abstracted away Helm charts and Helm chart configurations</a> into <code>HelmChart</code> and <code>HelmChartConfig</code> CRDs, for better or worse. I prefer to keep things as vanilla as possible, so we decided to leverage <code>HelmChartConfig</code> to add the necessary annotation.</p>
<p>Let's assume that our single available IP for MetalLB to hand out is <code>10.0.10.10</code>. Our <code>HelmChartConfig</code> would look something like this:</p>
<pre><code class="language-yaml">apiVersion: helm.cattle.io/v1
kind: HelmChartConfig
metadata:
  name: traefik
  namespace: kube-system
spec:
  valuesContent: |-
    service:
      enabled: true
      type: LoadBalancer
      annotations: metallb.universe.tf/allow-shared-ip: our-shared-ip
    spec:
      loadBalancerIP: "10.0.10.10"
</code></pre>
<p class="alert alert-info">
  The value of the annotation, <code>our-shared-ip</code>, just needs to be the same across the annotations. As far as I can tell, the reason for this is so that cluster administrators can section off access to sharing specific IPs by using a unique value per IP address. We chose a slightly more descriptive name to better indicate what's being configured to our future selves.
</p>
<p>We used <code>kubectl apply</code> to deploy our configuration and waited for k3s to redeploy Traefik, after which we verified that Traefik's <code>LoadBalancer</code> service had the proper annotation.</p>

<h3>Configuring MariaDB Galera for MetalLB IP Sharing</h3>
<p>Next, we needed to modify the <code>values.yaml</code> providing the configuration for the MariaDB Galera Helm chart. This is a much less involved process, as the Helm chart already accepts values to custom the service resource.</p>
<p>Again, assuming our single available IP for MetalLB to hand out is <code>10.0.10.10</code> and our annotation value is <code>our-shared-ip</code>, our <code>values.yaml</code> would contain a fragment similar to this:</p>
<pre><code class="language-yaml"># ...
service:
  type: LoadBalancer
  port: 3306
  loadBalancerIP: "10.0.10.10"
  annotations:
    metallb.universe.tf/allow-shared-ip: our-shared-ip
# ...
</code></pre>
<p>After modifying our <code>values.yaml</code>, we could either <code>helm upgrade</code> our existing installation of MariaDB Galera or tear it down and <code>helm install</code> from scratch in order to make the change. We opted to start over, since the database had yet to be modified.</p>

<h2>Results</h2>
<p>Finally, after configuring the annotations on both Traefik and MariaDB Galera, we looked at the services in our cluster. Both applications had services of type <code>LoadBalancer</code> with external IPs of <code>10.0.10.10</code>:</p>
<samp><pre><code class="language-plaintext">[root@vm /]# kubectl get svc --all-namespaces
NAMESPACE      NAME       TYPE            CLUSTER-IP      EXTERNAL-IP    PORT(S)                       AGE
...
kube-system    traefik    LoadBalancer    10.43.148.64    10.0.10.10     80:31570/TCP,443:30813/TCP    1d
...
mariadb        mariadb    LoadBalancer    10.43.64.58     10.0.10.10     3306:30260/TCP                1d
...
</code></pre></samp>
<p>We then verified that various addresses would resolve how we desired. Requests to <code>dns-for-my-cluster.domain.example:3306</code>, <code>10.0.10.10:3306</code>, and to any <code>nodeIP:30260</code> combination (a random port assigned by Kubernetes, like <code>NodePort</code>) were all met with responses from MariaDB Galera. Further, traffic sent to any of those addresses was being routed to any of the available MariaDB Galera pods, giving us high availability no matter how a client was configured. At this point, we were satisfied with our results.</p>

<p>Getting this configured was very much a team effort and I got to experience first hand some quirks of running applications in a kubernetes cluster designed before the containerized microservice paradigm became mainstream. Despite MetalLB pretty plainly mentioning this ability in its documentation, I don't think IP address sharing is a very common configuration scenario, and I hope this post serves to help others see what options they have for deploying MariaDB Galera in a bare metal, k3s flavored, production environment cluster.</p>
