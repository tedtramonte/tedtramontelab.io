const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");
const Image = require("@11ty/eleventy-img");
const lazyImagesPlugin = require('eleventy-plugin-lazyimages');
const path = require("path").posix;
const pluginRss = require("@11ty/eleventy-plugin-rss");

async function img(image) {
    if (image.src === undefined) {
        throw new Error(`Missing \`src\` attribute on image`);
    }
    if (image.alt === undefined) {
        throw new Error(`Missing \`alt\` attribute on image ${src}`);
    }
    if (image.class === undefined) {
        image.class = '';
    }
    if (image.style === undefined) {
        image.style = '';
    }
    if (image.widths === undefined) {
        image.widths = [null];
    }
    if (image.formats === undefined) {
        image.formats = [null];
    }
    if (image.link === undefined) {
        image.link = false;
    }

    relPath = path.parse(image.src);

    srcPath = path.parse(image.src);
    inputDir = srcPath.dir.split('/');
    inputDir[0] = 'src';
    inputDir.unshift('.');
    srcPath.dir = inputDir.join('/');

    publicPath = path.parse(image.src);
    outputDir = publicPath.dir.split('/');
    outputDir[0] = 'public';
    outputDir.unshift('.');
    publicPath.dir = outputDir.join('/');

    let options = {
        widths: image.widths,
        formats: image.formats,
        filenameFormat: function (id, src, width, format, options) {
            const extension = path.extname(src);
            const name = path.basename(src, extension);
            // TODO: maybe don't rename if width is null/the default?
            // linking to an image is hard otherwise
            return `${name}-${width}w.${format}`;
        },
        outputDir: `${publicPath.dir}/`,
        // useCache: false,
        urlPath: `${relPath.dir}/`,
    };

    console.log(`Generating image for ${image.src} at ${publicPath.dir}`);
    let metadata = await Image(path.format(srcPath), options);

    html = [];
    baseImgType = 'jpeg';
    if ('svg' in metadata) {
        baseImgType = 'svg';
    }
    if ('avif' in metadata) {
        baseImgType = 'avif';
    }
    if ('webp' in metadata) {
        baseImgType = 'webp';
    }
    if ('png' in metadata) {
        baseImgType = 'png';
    }
    if ('jpeg' in metadata) {
        baseImgType = 'jpeg';
    }

    if (image.link === true) {
        html.push(`<a
            class="dyn-img-link" href="#"
            data-pswp-width="${metadata[baseImgType].map(entry => entry.width).sort().reverse()[0]}"
            data-pswp-height="${metadata[baseImgType].map(entry => entry.height).sort().reverse()[0]}"
        >`);
    }
    html.push('<picture>');
    if ('svg' in metadata) {
        html.push(`  <source srcset="${metadata.svg.map(entry => entry.srcset).join(", ")}" type="${metadata.svg[0].sourceType}">`);
    }
    if ('avif' in metadata) {
        html.push(`  <source srcset="${metadata.avif.map(entry => entry.srcset).join(", ")}" type="${metadata.avif[0].sourceType}">`);
    }
    if ('webp' in metadata) {
        html.push(`  <source srcset="${metadata.webp.map(entry => entry.srcset).join(", ")}" type="${metadata.webp[0].sourceType}">`);
    }
    if ('png' in metadata) {
        html.push(`  <source srcset="${metadata.png.map(entry => entry.srcset).join(", ")}" type="${metadata.png[0].sourceType}">`);
    }
    if ('jpeg' in metadata) {
        html.push(`  <source srcset="${metadata.jpeg.map(entry => entry.srcset).join(", ")}" type="${metadata.jpeg[0].sourceType}">`);
    }
    html.push(`<img alt="${image.alt}" class="${image.class}" decoding="async" src="${metadata[baseImgType][0].url}" style="${image.style}">`);
    html.push('</picture>');
    if (image.link === true) {
        html.push('</a>');
    }
    return html.join('\n');
}

async function background_image(image) {
    if (image.src === undefined) {
        throw new Error(`Missing \`src\` attribute on image`);
    }
    if (image.formats === undefined) {
        image.formats = [null];
    }

    relPath = path.parse(image.src);

    srcPath = path.parse(image.src);
    inputDir = srcPath.dir.split('/');
    inputDir[0] = 'src';
    inputDir.unshift('.');
    srcPath.dir = inputDir.join('/');

    publicPath = path.parse(image.src);
    outputDir = publicPath.dir.split('/');
    outputDir[0] = 'public';
    outputDir.unshift('.');
    publicPath.dir = outputDir.join('/');

    let options = {
        widths: [null],
        formats: image.formats,
        filenameFormat: function (id, src, width, format, options) {
            const extension = path.extname(src);
            const name = path.basename(src, extension);
            return `bg-${name}.${format}`;
        },
        outputDir: `${publicPath.dir}/`,
        // useCache: false,
        urlPath: `${relPath.dir}/`,
    };

    console.log(`Generating background image for ${image.src} at ${publicPath.dir}`);
    let metadata = await Image(path.format(srcPath), options);

    webkitImageSetCss = ['background-image: -webkit-image-set('];
    webkitImageSet = [];
    imageSetCss = ['background-image: image-set('];
    imageSet = [];
    baseImgType = 'jpeg';
    if ('svg' in metadata) {
        webkitImageSet.push(`url('${metadata.svg[0].url}') type('${metadata.svg[0].sourceType}')`);
        imageSet.push(`url('${metadata.svg[0].url}') type('${metadata.svg[0].sourceType}')`);
        baseImgType = 'svg';
    }
    if ('avif' in metadata) {
        webkitImageSet.push(`url('${metadata.avif[0].url}') type('${metadata.avif[0].sourceType}')`);
        imageSet.push(`url('${metadata.avif[0].url}') type('${metadata.avif[0].sourceType}')`);
        baseImgType = 'avif';
    }
    if ('webp' in metadata) {
        webkitImageSet.push(`url('${metadata.webp[0].url}') type('${metadata.webp[0].sourceType}')`);
        imageSet.push(`url('${metadata.webp[0].url}') type('${metadata.webp[0].sourceType}')`);
        baseImgType = 'webp';
    }
    if ('png' in metadata) {
        webkitImageSet.push(`url('${metadata.png[0].url}') type('${metadata.png[0].sourceType}')`);
        imageSet.push(`url('${metadata.png[0].url}') type('${metadata.png[0].sourceType}')`);
        baseImgType = 'png';
    }
    if ('jpeg' in metadata) {
        webkitImageSet.push(`url('${metadata.jpeg[0].url}') type('${metadata.jpeg[0].sourceType}')`);
        imageSet.push(`url('${metadata.jpeg[0].url}') type('${metadata.jpeg[0].sourceType}')`);
        baseImgType = 'jpeg';
    }
    webkitImageSetCss = webkitImageSetCss.concat(webkitImageSet.join(','));
    webkitImageSetCss.push(');');
    imageSetCss = imageSetCss.concat(imageSet.join(','));
    imageSetCss.push(');');
    fallbackCss = [`background-image: url('${metadata[baseImgType][0].url}');`];
    css = fallbackCss.concat(webkitImageSetCss, imageSetCss);
    return css.join('');
}

module.exports = function (config) {

    // Non-image, JS, or CSS files need to be copied here
    config.addPassthroughCopy('src/assets/pdf');
    config.addPassthroughCopy('src/assets/img/favicon');

    config.setDataDeepMerge(true);

    config.addPlugin(eleventyNavigationPlugin);
    config.addPlugin(lazyImagesPlugin, {
        appendInitScript: false, // Don't inject lazysizes, webpack bundles it
        // cacheFile: '', // Don't cache results to a file
        maxPlaceholderHeight: 16,
        maxPlaceholderWidth: 16,
        setWidthAndHeightAttrs: true,
        transformImgPath: (src, options) => {
            if (
                src.startsWith('./') ||
                src.startsWith('../') ||
                (!src.startsWith('/') &&
                    !src.startsWith('http://') &&
                    !src.startsWith('https://') &&
                    !src.startsWith('data:'))
            ) {
                // The file path is relative to the output document
                const outputDir = path.posix.parse(options.inputPath).dir;
                return path.posix.normalize(outputDir + '/' + src);
            }

            // Refer to output file
            if (src.startsWith('/') && !src.startsWith('//')) {
                return `./public${src}`;
            }

            return src;
        }
    });
    config.addPlugin(pluginRss);

    config.addShortcode('contact_button', function () {
        return `
            <div class="position-fixed position-bottom-right d-none d-lg-block">
                <a class="btn btn-lg btn-primary shadow" href="/contact">Contact Ted</a>
            </div>
        `;
    });

    config.addNunjucksAsyncShortcode("img", img);
    config.addLiquidShortcode("img", img);
    config.addJavaScriptFunction("img", img);
    config.addNunjucksAsyncShortcode("background_image", background_image);
    config.addLiquidShortcode("background_image", background_image);
    config.addJavaScriptFunction("background_image", background_image);

    // Custom filter and collection for handling post tags
    // https://github.com/11ty/eleventy-base-blog/blob/master/.eleventy.js
    function filterTagList(tags) {
        return (tags || []).filter(tag => ["all", "nav", "post", "posts"].indexOf(tag) === -1);
    }
    config.addFilter("filterTagList", filterTagList);

    config.addCollection("blogTags", function (collection) {
        let tagSet = new Set();
        collection.getAll().forEach(item => {
            (item.data.tags || []).forEach(tag => tagSet.add(tag));
        });

        let filteredTags = filterTagList([...tagSet]);
        let blogTags = [];
        filteredTags.forEach(tag => {
            blogTags.push({
                name: tag,
                count: collection.getFilteredByTag(tag).length,
            });
        });
        return blogTags;
    });

    // Custom filter for getting the first 'n' elements of a collection
    // https://github.com/11ty/eleventy-base-blog/blob/master/.eleventy.js
    config.addFilter("head", (array, n) => {
        if (n < 0) {
            return array.slice(n);
        }

        return array.slice(0, n);
    });

    return {
        dir: {
            input: "src",
            output: "public",
            includes: "_includes",
            layouts: "_layouts",
        },
        templateFormats: [
            'html',
            'njk',
            'md',
            '11ty.js'
        ],
        htmlTemplateEngine: "njk"
    };
};
