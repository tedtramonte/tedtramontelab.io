const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  devtool: (process.env.NODE_ENV === 'development') ? 'inline-source-map' : false,
  mode: process.env.NODE_ENV,
  entry: {
    critical: [
      './src/assets/js/critical.js',
    ],
    index: [
      './src/assets/js/index.js',
      './src/assets/js/dark_mode.js',
      './src/assets/scss/style.scss',
    ],
    projects: [
      './src/assets/js/projects.js',
    ],
    photography: [
      './src/assets/js/photography.js',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'public', 'assets'),
    filename: "js/[name].js",
    publicPath: "/"
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/style.css',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/,
        use:
          [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { sourceMap: true } },
            { loader: 'sass-loader', options: { sourceMap: true } },
          ],
      },
    ],
  },
};
